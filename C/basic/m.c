/*************************************************************************
	> File Name: m.c
	> Author: Hale Lv
	> Mail: Aspire_8@163.com 
	> Created Time: 2019年09月21日 星期六 17时48分19秒
 ************************************************************************/

#include<stdio.h>

int main(){
	d();
	a();
	fo();
	whiles();
	dowhile();
	fors();
	int i = 1, sum = 0;
	while(i <= 100){
		sum += i;
		i++;
	}
	printf("sum is: %d\n", sum);
	return 0;
}

int fo(){
	int i = 0,sum = 0;
	for(i=1; i <= 100; i++){
		sum += i;
	}
	printf("The sums is %d\n", sum);
	return 0;
}

int a(){
	int n = 0;
	printf("Input a string :");
	while(getchar()!='\n') n++;
	printf("Number of characters : %d\n",n);
	return 0;
}

int d(){
	int i = 0, sum = 0;
	do{
		sum += i;
		i++;
	}while(i < 100);
	printf("the sum is : %d\n",sum);
	return 0;
}

int whiles(){
	int i = 0, sum = 0;
	while(i < 100){
		sum += i;
		i++;
	}
	printf("while sum %d\n",sum);
	return 0;
}


int dowhile(){
	int i, sum = 0;
	do{
		sum += i;
		i++;
	}while(i < 100);
	printf("the do while sum is %d\n",sum);
	return 0;
}

int fors(){
	int i, sum = 0;
	for(i = 1; i < 100; i++){
		sum += i;
		i++;
	}
	printf("the for sum is %d\n",sum);
	return 0;
}


