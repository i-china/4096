/*************************************************************************
	> File Name: D.java
	> Author: Hale Lv
	> Mail: Aspire_8@163.com 
	> Created Time: 2019年09月07日 星期六 15时45分53秒
 ************************************************************************/

public class D {
	public static void main(String[] args) {
		Aoo(12);
		int age = 12;
		if(5 > 2){
			int s = 3;
			System.out.println("s" + s);
			System.out.println("s" + age);
		}
		// System.out.println('s' + s);
		C c = new C();
		System.out.println("name " + c.name);
		System.out.println("age " + c.age);
		Boo();
	}
	public static void Aoo(int n){
		System.out.println("n " + n);
	}
	public static void Boo(){
		try{
			System.out.println("Hello exception");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}

