/*************************************************************************
	> File Name: E.java
	> Author: Hale Lv
	> Mail: Aspire_8@163.com 
	> Created Time: 2019年09月07日 星期六 16时03分56秒
 ************************************************************************/
 
public class E {
	public static void main(String[] args) {
		byte a = 12;
		short s = 12;
		int i = 123;
		long l = 1234;
		long sun = a + s + i + l;
		System.out.println(sun);
		Foo();
	}
	public static void Foo(){
		float f = 10.23f;
		double d = 123.321;
		double sum = f + d;
		System.out.println("sum " + sum);
	}
}
