##########################################################################
# File Name: a.sh
# Author: Hale Lv
# mail: Aspire_8@163.com
# Created Time: 2019年09月21日 星期六 10时36分32秒
#########################################################################
#!/bin/bash
a=(one two three)
echo ${#a[@]}
echo ${#a[*]}

b=(on won three four)
unset b[1]
echo ${b[@]}
unset b
echo ${b[*]}

read a
read b 

if (( $a == $b))
then 
	echo 'a 等于 b'
fi

i=1
sum=0
while ((i <= 100))
do 
	((sum += i))
	(( i++ ))
done
echo "The sum is: $sum"

s=0
for ((i=1; i<=100;i++))
do 
	((s+=i))
done
echo "The sum is : $s"

ss=0
for ((i=1;i <= 100;i++))
do 
	if (( i> 100)); then 
		break
	fi 
	((ss+=i))
done
echo "the sum is : $ss"

sss=0
for n in 1 2 3 4 5 
do 
	echo $n
	(( sss += n))
done
echo "The sum is: $sss"

si=0
for n in {1..100}
do 
	(( si += n))
done
echo $si
 
echo "Enter your name"
select name in "PHP " "PYTHON" "SHELL" "LINUX" "MAC" "WINDOWS"
do 
	echo $name
done
echo "Your select name is: $name"
