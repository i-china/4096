<?php

// 获取用 GET ， 提交用POST
// Posts list | 获取文章列表 
Route::get('/posts','\App\Http\Controllers\PostController@index');

// Create Post | Store 存储
Route::get('/posts/create','\App\Http\Controllers\PostController@create');
Route::post('/posts','\App\Http\Controllers\PostController@store');

// Posts info | 获取信息
Route::get('/posts/{post}','\App\Http\Controllers\PostController@show');

// Edit Post  | Update  更新
Route::get('/posts/{post}/edit','\App\Https\Controllers\PostController@edit');
Route::post('/posts/{post}','\App\Http\Controllers\PostController@update');

// Delete Post  | Delete 删除
Route::get('/posts/delete','\App\Http\Controllers\PostController@delete');




