<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{
    // 列表
	public function index(){
		$titles = [
			['title' => 'PHP Laravel'],
			['title' => 'Python Danjo'],
			['title' => 'Golang Docker'],
		];
		return view("post/index",['titles' => $titles ]);
	}

	// 详情
	public function show(){
		return view('post/show');
	}

	// 创建
	public function create(){
		return view('post/create');	
	}
	// 存储
	public function store(){
		return view('post/store');	
	}
	// 编辑
	public function edit(){
		return view('post/edit');
	}
	// 更新
	public function update(){
		return view('post/update');
	}
	// 删除
	public function delete(){
		return view('post/delete');
	}
}
